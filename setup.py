import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    requirements = fh.readlines()

setuptools.setup(
    name='CustomLogs',
    version='0.1',
    description='Redirect stdout and stderr to both the console and a file',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='V',
    packages=setuptools.find_packages(),
    install_requires=requirements,
)
