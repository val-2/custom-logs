# Custom Logs
Python library to redirect stdout and stderr to both the console and a file

## Usage
Just do 
```
import customlogs
customlogs.custom_logs()
```

## Known bugs
- Garbled text in the console when using ```print```. Please use ```safe_print``` or put at the start of your code:
```
  from custom_logs import print
```
