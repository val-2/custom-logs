import builtins
import getpass
import io
import logging
import pathlib
import sys
import warnings


class Interceptor:
    def __init__(self, intercepted):
        self.intercepted = intercepted

    def __getattr__(self, item):
        first_attribute = getattr(self.intercepted[0], item)
        other_attributes = [getattr(i, item) for i in self.intercepted[1:]]
        if hasattr(first_attribute, '__call__'):
            def func(*args, **kwargs):
                result = first_attribute(*args, **kwargs)
                for attribute in other_attributes:
                    attribute(*args, **kwargs)
                return result

            return func
        else:
            return first_attribute


def custom_logs(logs_filename=None, file=None, max_logs_lines=3000, log_date_format='%Y-%m-%d %H:%M:%S',
                log_format=f'[%(levelname)s] %(asctime)s {sys.platform} {getpass.getuser()} %(module)s:%(lineno)d %(message)s'):
    if isinstance(sys.stdout, Interceptor):
        warnings.warn('Warning: custom_logs called more than once')
        return

    if file is not None and logs_filename is None:
        file_path = pathlib.Path(file)
        logs_filename = file_path.parent.parent.absolute() / f'{file_path.stem}.log'
    elif file is None and logs_filename is not None:
        if not isinstance(logs_filename, pathlib.Path):
            logs_filename = pathlib.Path(logs_filename)
        elif logs_filename is None:
            logs_filename = pathlib.Path(__file__).parent.parent.absolute() / 'logs.log'
    elif file is not None and logs_filename is not None:
        raise ValueError('You cannot specify both file and logs_filename')
    else:
        raise ValueError('You must specify either file or logs_filename')

    logs_filename.parent.mkdir(parents=True, exist_ok=True)
    logs_filename.touch(exist_ok=True)

    if max_logs_lines:
        with open(logs_filename, 'r+') as f:
            data = f.readlines()
            if len(data) > max_logs_lines:
                f.seek(0)
                f.writelines(data[-max_logs_lines:])
                f.truncate()

    stds = io.StringIO()
    logs_file = open(logs_filename, 'a', encoding='UTF-8', buffering=1)
    sys.stdout = Interceptor([sys.stdout, stds, logs_file])
    sys.stderr = Interceptor([sys.stderr, stds, logs_file])

    logging.basicConfig(level=20, datefmt=log_date_format, format=log_format, stream=sys.stdout)


def get_logs_since_start() -> str:
    try:
        return sys.stdout.intercepted[1].getvalue()
    except AttributeError:
        raise AttributeError('You must call custom_logs() before calling get_logs_since_start()')


def safe_print(*args, sep=" ", end="\n", file=None, **kwargs):
    builtins.print(f'{sep.join([str(arg) for arg in args])}{end}', sep=sep, end='', file=file, **kwargs)


print = safe_print
